package com.example.shapeslist;

import com.example.shapeslist.controller.ShapesListController;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import com.example.shapeslist.entities.Shape;
import com.example.shapeslist.entities.Square;
import com.example.shapeslist.entities.Circle;
import com.example.shapeslist.entities.TriangleRightAngle;
import com.example.shapeslist.entities.TriangleEquilateral;
import com.example.shapeslist.entities.TriangleScalene;


import java.util.ArrayList;

@SpringBootTest
class ShapesListApplicationTests {

	ShapesListController controller;

	@BeforeEach void setup(){
		controller = new ShapesListController();
	}

	@Test
	void squareTest() {
		Shape testShape = new Square(5.0);
		assertEquals(25.0, testShape.getArea());
	}

	@Test
	void circleTest() {
		Shape testShape = new Circle(5.0);
		assertEquals(Math.PI*Math.pow(5,2), testShape.getArea());
	}

	@Test
	void triangleRightAngleTest(){
		Shape testShape = new TriangleRightAngle(5.0, 4.0);
		assertEquals(10.0,testShape.getArea());
	}

	@Test
	void triangleEquilateralTest(){
		Shape testShape = new TriangleEquilateral(5.0);
		assertEquals((Math.pow(3,0.5)/4)*5,testShape.getArea());
	}

	@Test
	void triangleScaleneTest(){
		Shape testShape = new TriangleScalene(1.0,2.0,3.0);
		Double s = (1+2+3)/2.0; // semi perimeter
		assertEquals(Math.pow(s*(s-1)*(s-2)*(s-3),0.5),testShape.getArea());
	}





}
