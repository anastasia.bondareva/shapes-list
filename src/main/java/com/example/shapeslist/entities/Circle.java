package com.example.shapeslist.entities;

public class Circle implements Shape{

    private Double radius;

    public Circle(Double radius){
        this.radius = radius;

    }

    @Override
    public Double getArea() {
        return Math.PI*Math.pow(this.radius,2);
    }
}
