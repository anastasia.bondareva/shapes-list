package com.example.shapeslist.entities;

public class TriangleRightAngle implements Shape{

    private Double width;
    private Double height;


    public TriangleRightAngle(Double height, Double width) {
        this.height = height;
        this.width = width;

    }

    @Override
    public Double getArea() {
        return this.height*this.width/2;
    }
}
