package com.example.shapeslist.entities;



public class Square implements Shape{

    private Double sideLength;

    public Square(Double sideLength) {
        this.sideLength = sideLength;

    }

    @Override
    public Double getArea() {
        return Math.pow(this.sideLength,2);
    }
}
