package com.example.shapeslist.entities;

public class TriangleScalene implements Shape{
    private Double a;
    private Double b;
    private Double c;


    public TriangleScalene(Double a,Double b,Double c){
        this.a = a;
        this.b = b;
        this.c = c;
        Double s = (a+b+c)/2; // semi perimeter


    }

    @Override
    public Double getArea() {
        Double s = (a+b+c)/2; // semi perimeter
        return Math.pow(s*(s-a)*(s-b)*(s-c),0.5);
    }
}
