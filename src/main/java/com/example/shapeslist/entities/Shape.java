package com.example.shapeslist.entities;

public interface Shape {
    public Double getArea();
}
