package com.example.shapeslist.entities;

public class TriangleEquilateral implements Shape{

    private Double sideLength;

public TriangleEquilateral(Double sideLength){
    this.sideLength = sideLength;
}

    @Override
    public Double getArea() {
        return (Math.pow(3,0.5)/4)*sideLength;
    }
}
