package com.example.shapeslist;

import com.example.shapeslist.entities.*;
import com.example.shapeslist.controller.ShapesListController;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShapesListApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShapesListApplication.class, args);
	}

}
