package com.example.shapeslist.controller;

import org.springframework.web.bind.annotation.*;

import com.example.shapeslist.entities.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/shapes-list")
public class ShapesListController {
    List<Shape> list = new ArrayList<>();


    @PostMapping("/add/square")
    public String addSquare(@RequestParam(name="width") Double sideLength){
        Shape toAdd = new Square(sideLength);
        list.add(toAdd);
        return "You have just added a Square with a width of "+sideLength;
    }

    @PostMapping("/add/circle")
    public String addCircle(@RequestParam(name="radius")Double radius){
        Shape toAdd = new Circle(radius);
        list.add(toAdd);
        return "You have just added a circle with a radius of "+radius;
    }

    @PostMapping("/add/right-angle-triangle")
    public String addTriangleRightAngle(@RequestParam(name="height")Double height,@RequestParam(name="width") Double width){
        Shape toAdd = new TriangleRightAngle(height, width);
        list.add(toAdd);
        return "You have just added a right angle triangle with a height of "+height+" and a width of"+width;
    }

    @PostMapping("/add/equilateral-triangle")
    public String addTriangleEquilateral(@RequestParam(name="side-length")Double sideLength){
        Shape toAdd = new TriangleEquilateral(sideLength);
        list.add(toAdd);
        return"You have just added an equilateral triangle with a side length of"+sideLength;
    }

    @PostMapping("/add/scalene-triangle")
    public String addTriangleScalene(@RequestParam(name="side1")Double a,@RequestParam(name="side2")Double b,@RequestParam(name="side3")Double c){
        Shape toAdd = new TriangleScalene(a,b,c);
        list.add(toAdd);
        return"You have just added an scalene triangle with sides of lengths "+a+", "+b+", and "+c;
    }


    @GetMapping("/get-area")
    public String getListArea(){
        Double sum = 0.0;
        for(Shape shape: list){
            sum += shape.getArea();
        }


        return "The area of all the shapes in the list combined is "+sum;
    }


}
